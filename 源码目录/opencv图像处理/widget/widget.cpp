﻿#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    path="D:\\Qt\\Qt5Book\\Qt5.9Samp\\login1\\login\\res\\pic\\2.png";
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_clicked()
{
    Mat srcimage= imread(path.toStdString());
    cvtColor(srcimage,srcimage,CV_BGR2RGB);
    QImage disimage=QImage(srcimage.data,srcimage.cols,srcimage.rows,srcimage.cols*srcimage.channels(),QImage::Format_RGB888);
    disimage = disimage.scaled(ui->oldlabel->width(),ui->oldlabel->height());
    ui->oldlabel->setPixmap(QPixmap::fromImage(disimage));

    //像素二维矩阵函数
    int rows = srcimage.rows;
    //像素二维矩阵列数
    int cols = srcimage.cols * srcimage.channels();
    qDebug()<<rows<<cols<<srcimage.channels();//600 1500 3
    for(int i=0;i<rows;i++)
    {
        uchar * data = srcimage.ptr<uchar>(i);
        for(int j=0;j<cols;j++)
        {
            //雪花屏特效
            int q = rand()%cols;
            data[q]=155;//某些通道随机改成155
        }
    }
    //Mat转QImage 像素   newlabel放置图像处理后图片
    QImage disimage2 = QImage(srcimage.data,srcimage.cols,srcimage.rows,srcimage.cols*srcimage.channels(),QImage::Format_RGB888);
    disimage2 = disimage2.scaled(ui->newlabel->width(),ui->newlabel->height());
    ui->newlabel->setPixmap(QPixmap::fromImage(disimage2));
}

void Widget::on_pushButton_2_clicked()
{
    //读取原始图片
    Mat srcimage = imread(path.toStdString());
    //Mat转QImage 颜色
    cvtColor(srcimage,srcimage,CV_BGR2RGB);
    //Mat转QImage 像素   oldlabel放置原图
    QImage disimage = QImage(srcimage.data,srcimage.cols,srcimage.rows,srcimage.cols*srcimage.channels(),QImage::Format_RGB888);

    Mat retimage;
    //高斯模糊
    GaussianBlur(srcimage,retimage,Size(5,5),0,0);

    //Mat转QImage 像素   newlabel放置图像处理后图片
    QImage disimage2 = QImage(retimage.data,retimage.cols,retimage.rows,retimage.cols*retimage.channels(),QImage::Format_RGB888);
    disimage2 = disimage2.scaled(ui->newlabel->width(),ui->newlabel->height());
    ui->newlabel->setPixmap(QPixmap::fromImage(disimage2));
}

void Widget::on_pushButton_3_clicked()
{
    //读取原始图片
    Mat srcimage = imread(path.toStdString());
    //Mat转QImage 颜色
    cvtColor(srcimage,srcimage,CV_BGR2RGB);
    //Mat转QImage 像素   oldlabel放置原图
    QImage disimage = QImage(srcimage.data,srcimage.cols,srcimage.rows,srcimage.cols*srcimage.channels(),QImage::Format_RGB888);
    disimage = disimage.scaled(ui->oldlabel->width(),ui->oldlabel->height());
    ui->oldlabel->setPixmap(QPixmap::fromImage(disimage));

    Mat retumage;
    //中值滤波
    medianBlur(srcimage,retumage,5);

    //Mat转QImage 像素   newlabel放置图像处理后图片
    QImage disimage2 = QImage(retumage.data,retumage.cols,retumage.rows,retumage.cols*retumage.channels(),QImage::Format_RGB888);
    disimage2 = disimage2.scaled(ui->newlabel->width(),ui->newlabel->height());
    ui->newlabel->setPixmap(QPixmap::fromImage(disimage2));

}

void Widget::on_pushButton_5_clicked()
{
    //读取原始图片
    Mat srcimage = imread(path.toStdString());
    //Mat转QImage 颜色
    cvtColor(srcimage,srcimage,CV_BGR2RGB);
    //Mat转QImage 像素   oldlabel放置原图
    QImage disimage = QImage(srcimage.data,srcimage.cols,srcimage.rows,srcimage.cols*srcimage.channels(),QImage::Format_RGB888);
    disimage = disimage.scaled(ui->oldlabel->width(),ui->oldlabel->height());
    ui->oldlabel->setPixmap(QPixmap::fromImage(disimage));

    Mat retumage;
    //灰度处理 灰度是单通道8位 QImage是24位三通道
    cvtColor(srcimage,retumage,CV_BGR2GRAY);
    cvtColor(retumage,retumage,CV_GRAY2BGR);

    //Mat转QImage 像素   newlabel放置图像处理后图片
    QImage disimage2 = QImage(retumage.data,retumage.cols,retumage.rows,retumage.cols*retumage.channels(),QImage::Format_RGB888);
    disimage2 = disimage2.scaled(ui->newlabel->width(),ui->newlabel->height());
    ui->newlabel->setPixmap(QPixmap::fromImage(disimage2));
}

void Widget::on_pushButton_6_clicked()
{

    //读取原始图片
    Mat srcimage = imread(path.toStdString());
    //Mat转QImage 颜色
    cvtColor(srcimage,srcimage,CV_BGR2RGB);
    //Mat转QImage 像素   oldlabel放置原图
    QImage disimage = QImage(srcimage.data,srcimage.cols,srcimage.rows,srcimage.cols*srcimage.channels(),QImage::Format_RGB888);
    disimage = disimage.scaled(ui->oldlabel->width(),ui->oldlabel->height());
    ui->oldlabel->setPixmap(QPixmap::fromImage(disimage));

    Mat retumage;
    //xy轴模糊
    blur(srcimage,retumage,Size(10,10));

    //Mat转QImage 像素   newlabel放置图像处理后图片
    QImage disimage2 = QImage(retumage.data,retumage.cols,retumage.rows,retumage.cols*retumage.channels(),QImage::Format_RGB888);
    disimage2 = disimage2.scaled(ui->newlabel->width(),ui->newlabel->height());
    ui->newlabel->setPixmap(QPixmap::fromImage(disimage2));
}

void Widget::on_pushButton_4_clicked()
{
    //读取原始图片
    Mat srcimage = imread(path.toStdString());
    //Mat转QImage 颜色
    cvtColor(srcimage,srcimage,CV_BGR2RGB);
    //Mat转QImage 像素   oldlabel放置原图
    QImage disimage = QImage(srcimage.data,srcimage.cols,srcimage.rows,srcimage.cols*srcimage.channels(),QImage::Format_RGB888);
    disimage = disimage.scaled(ui->oldlabel->width(),ui->oldlabel->height());
    ui->oldlabel->setPixmap(QPixmap::fromImage(disimage));

    //毛玻璃
    RNG rng;
    int random;
    int num = 5;
    for(int i=0;i<srcimage.rows -num;i++)
    {
        for(int j=0;j<srcimage.cols -num;j++)
        {
            //通过rng返回0-15随机数
            random = rng.uniform(0,num);
            srcimage.at<Vec3b>(i,j)[0] = srcimage.at<Vec3b>(i+random,j+random)[0];
            srcimage.at<Vec3b>(i,j)[1] = srcimage.at<Vec3b>(i+random,j+random)[1];
            srcimage.at<Vec3b>(i,j)[2] = srcimage.at<Vec3b>(i+random,j+random)[2];
        }
    }

    //Mat转QImage 像素   newlabel放置图像处理后图片
    QImage disimage2 = QImage(srcimage.data,srcimage.cols,srcimage.rows,srcimage.cols*srcimage.channels(),QImage::Format_RGB888);
    disimage2 = disimage2.scaled(ui->newlabel->width(),ui->newlabel->height());
    ui->newlabel->setPixmap(QPixmap::fromImage(disimage2));
}

void Widget::on_pushButton_7_clicked()
{
    //读取原始图片
    Mat srcimage = imread(path.toStdString());
    //Mat转QImage 颜色
    cvtColor(srcimage,srcimage,CV_BGR2RGB);
    //Mat转QImage 像素   oldlabel放置原图
    QImage disimage = QImage(srcimage.data,srcimage.cols,srcimage.rows,srcimage.cols*srcimage.channels(),QImage::Format_RGB888);
    disimage = disimage.scaled(ui->oldlabel->width(),ui->oldlabel->height());
    ui->oldlabel->setPixmap(QPixmap::fromImage(disimage));

    Mat retumage;
    //双倍模糊
    cv::bilateralFilter(srcimage,retumage,15,120,10,4);

    //Mat转QImage 像素   newlabel放置图像处理后图片
    QImage disimage2 = QImage(retumage.data,retumage.cols,retumage.rows,retumage.cols*retumage.channels(),QImage::Format_RGB888);
    disimage2 = disimage2.scaled(ui->newlabel->width(),ui->newlabel->height());
    ui->newlabel->setPixmap(QPixmap::fromImage(disimage2));
}

void Widget::on_pushButton_8_clicked()
{
    //读取原始图片
    Mat srcimage = imread(path.toStdString());
    //Mat转QImage 颜色
    cvtColor(srcimage,srcimage,CV_BGR2RGB);
    //Mat转QImage 像素   oldlabel放置原图
    QImage disimage = QImage(srcimage.data,srcimage.cols,srcimage.rows,srcimage.cols*srcimage.channels(),QImage::Format_RGB888);
    disimage = disimage.scaled(ui->oldlabel->width(),ui->oldlabel->height());
    ui->oldlabel->setPixmap(QPixmap::fromImage(disimage));

    Mat retumage;
    //腐蚀
    Mat element = cv::getStructuringElement(MORPH_RECT,Size(5,5));
    cv::erode(srcimage,retumage,element);

    //Mat转QImage 像素   newlabel放置图像处理后图片
    QImage disimage2 = QImage(retumage.data,retumage.cols,retumage.rows,retumage.cols*retumage.channels(),QImage::Format_RGB888);
    disimage2 = disimage2.scaled(ui->newlabel->width(),ui->newlabel->height());
    ui->newlabel->setPixmap(QPixmap::fromImage(disimage2));
}
