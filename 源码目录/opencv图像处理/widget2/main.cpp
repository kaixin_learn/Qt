﻿#include <QApplication>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <QDebug>
using namespace cv;
using namespace std;

void hsv_test(string fname){
    Mat img = imread(fname);
    resize(img,img,Size(300,500),0,0,INTER_AREA);//调整图片大小
    /*resize(InputArray src, OutputArray dst, Size dsize,
    double fx=0, double fy=0, int interpolation=INTER_LINEAR )
    INTER_AREA:一种插值的方式
    fx：width方向的缩放比例，如果它是0，那么它就会按照(double)dsize.width/src.cols来计算；
    fy：height方向的缩放比例，如果它是0，那么它就会按照(double)dsize.height/src.rows来计算；
    参考：https://blog.csdn.net/xidaoliang/article/details/86504720
    */
    imshow("src", img);
    cvtColor(img,img,COLOR_BGR2HSV);//BGR--->HSV
    vector<Mat> channels;
    split(img,channels);
    for (int i = 0; i < img.rows; i++){
    //每一行
    //分离出3个通道,channels[0]代表h
        unsigned char* data = channels[0].ptr<unsigned char>(i);
        //每一行第一个元素的指针
        //unsigned char:8bit
        for (int j = 0; j < img.cols; j++){//每一个
            int temp = data[j];
            //h的范围：0---180
            //蓝色的h值范围在:100---124
            //红色的h值范围在156---180
            //这句是蓝色变红色：
            if(temp<=124 && temp>=100)temp = 180;



            //红色的h值范围在156---180
            //绿色的h值范围在35---77
            //这句是红色变绿色
            if(temp>=156 && temp<=180)temp=77;
            data[j] = temp;
        }
    }
    Mat result;
    merge(channels, result);
    cvtColor(result, result, COLOR_HSV2BGR);//HSV--->BGR
    imshow("result", result);
    waitKey();
}
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    string path="D:\\Qt\\Qt5Book\\Qt5.14\\opencv\\try1\\widget\\5.png";
    hsv_test(path);
    return a.exec();
}
