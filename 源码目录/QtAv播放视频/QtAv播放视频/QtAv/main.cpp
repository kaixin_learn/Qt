﻿#include "qtav.h"
#include <QApplication>
#include <WidgetRenderer.h>
#include <QtAV>
#include <QtAVWidgets>
#include <QtAV/AVPlayer.h>

using namespace QtAV;
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
//    QtAv w;
//    w.show();
    WidgetRenderer renderer;
    renderer.show();
    Widgets::registerRenderers();
    AVPlayer player;
    player.setRenderer(&renderer);
    player.play("02.mp4");
    return a.exec();
}
