#ifndef QTAV_H
#define QTAV_H

#include <QWidget>

namespace Ui {
class QtAv;
}

class QtAv : public QWidget
{
    Q_OBJECT

public:
    explicit QtAv(QWidget *parent = 0);
    ~QtAv();

private:
    Ui::QtAv *ui;
};

#endif // QTAV_H
