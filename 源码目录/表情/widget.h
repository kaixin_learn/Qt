#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    void addEmotionItem(int row,int column,QString fileName);
    void initEmotion();
private:
    Ui::Widget *ui;
    QString path;
};

#endif // WIDGET_H
