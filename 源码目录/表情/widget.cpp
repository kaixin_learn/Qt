#include "widget.h"
#include "ui_widget.h"
#include <QLabel>
#include <QMovie>
#include <QDebug>
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    path=QString(":/qq_emoji/%1@2x.gif");
    initEmotion();

    //一个问题是gif的位置在左边，不是据中的。图片也是，是图片的问题吗？不是，是label设置问题。
    QMovie* movie=new QMovie;
    //QSize si(ui->label->size());
    movie->setFileName(path.arg(3));
    //movie->setScaledSize(si);
    ui->label->setMovie(movie);
    ui->label->setScaledContents(true);
    ui->label->setAlignment(Qt::AlignCenter);
    movie->start();
    //qDebug()<<movie->scaledSize();

    /*
    QMovie* movie2=new QMovie;
    //QSize si2(ui->label_2->size());
    movie2->setFileName(path.arg(3));
    //movie2->setScaledSize(si2);
    ui->label_2->setMovie(movie2);
    ui->label_2->setScaledContents(true);
    movie2->start();
    */
    QPixmap pix(":/qq_emoji/100fix@2x.png");
    pix=pix.scaled(QSize(ui->label_2->size()));
    ui->label_2->setPixmap(pix);
    ui->label_2->setAlignment(Qt::AlignCenter);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::addEmotionItem(int row,int column,QString fileName)
{
    QTableWidgetItem* tableWidgetItem=new QTableWidgetItem;
    ui->tableWidget->setItem(row,column,tableWidgetItem);;

    QLabel* emotionIcon=new QLabel;
    emotionIcon->setMargin(4);
    QMovie* movie=new QMovie;
    movie->setFileName(fileName);
    movie->start();
    emotionIcon->setMovie(movie);
    emotionIcon->setScaledContents(true);
    emotionIcon->setAlignment(Qt::AlignCenter);
    ui->tableWidget->setCellWidget(row,column,emotionIcon);
    //qDebug()<<emotionIcon->size();
}

void Widget::initEmotion()
{
    //setWindowFlags(Qt::FramelessWindowHint);
    for(int i=0;i<4;i++)
        for(int j=0;j<4;j++)
        {
            addEmotionItem(i,j,path.arg(i*4+j+2));
        }
}

