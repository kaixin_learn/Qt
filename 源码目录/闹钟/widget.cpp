﻿#pragma execution_character_set(“utf-8”)
#include "widget.h"
#include "ui_widget.h"
#include <QTimer>
#include <QTime>
#include <QUrl>
#include <QFileDialog>
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    setWindowTitle("时钟");
    setWindowIcon(QIcon(":/music/时钟.png"));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_timertimeout()
{
    rest--;
    int min=rest/60;
    int sec=rest%60;
    ui->lcdNumbermin->display(min);
    ui->lcdNumbersec->display(sec);
    //每过一秒，对应显示板上也过一秒。
}

void Widget::on_pushButton_clicked()
{
    rest=ui->spinBox->value()*60;
    studyTime=0;

    Timer=new QTimer(this);
    Timer->setInterval(1000);
    connect(Timer,SIGNAL(timeout()),this,SLOT(on_timertimeout()));
    Timer->start();

    overMusic=new QTimer(this);
    overMusic->setInterval(1000*ui->spinBox->value()*60);
    connect(overMusic,SIGNAL(timeout()),this,SLOT(on_overmusictimeout()));
    overMusic->start();

    QTime curTime=QTime::currentTime();
    ui->startTime->setText("开始时间: "+curTime.toString("hh时mm分ss秒"));
    QTime over;
    over=curTime.addSecs(ui->spinBox->value()*60);
    ui->overTime->setText("结束时间: "+over.toString("hh时mm分ss秒"));    
}

void Widget::on_overmusictimeout()
{
    player=new QMediaPlayer(this);
    QString File="C:/Users/连沛亮/Music/01.mp3";
    player->setMedia(QUrl::fromLocalFile(File));
    player->play();
    studyTime+=ui->spinBox->value();
}
