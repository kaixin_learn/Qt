#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QMediaPlayer>
namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;
    QTimer *Timer;
    QTimer *overMusic;
    int rest;
    QMediaPlayer *player;
    int studyTime;
private slots:
    void on_timertimeout();
    void on_pushButton_clicked();
    void on_overmusictimeout();
};

#endif // WIDGET_H
