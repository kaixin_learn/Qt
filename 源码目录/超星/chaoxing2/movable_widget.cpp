﻿#include "movable_widget.h"
#include <QMouseEvent>
#include <QPainter>
movable_widget::movable_widget(QWidget * parent):
    QWidget(parent)
{
    setWindowFlag(Qt::FramelessWindowHint);
    setAttribute(Qt::WA_TranslucentBackground);

}
void movable_widget::mouseMoveEvent(QMouseEvent *event)
{
    if(ismoving)
    {
        QPoint now=event->globalPos()-Pos;
        move(now);
    }
    QWidget::mouseMoveEvent(event);
}

void movable_widget::mousePressEvent(QMouseEvent *event)
{
    if(event->button()==Qt::LeftButton)
    {
            ismoving=true;
            Pos=event->globalPos()-pos();
    }
    QWidget::mousePressEvent(event);
}

void movable_widget::mouseReleaseEvent(QMouseEvent *event)
{
    ismoving=false;
    QWidget::mouseReleaseEvent(event);
}
void movable_widget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);	// 反锯齿;
    painter.setBrush(QBrush(Qt::red));
    painter.setPen(Qt::transparent);
    QRect rect = this->rect();
    rect.setWidth(rect.width() - 1);
    rect.setHeight(rect.height() - 1);
    painter.drawRoundedRect(rect, 15, 15);
    QWidget::paintEvent(event);
}



