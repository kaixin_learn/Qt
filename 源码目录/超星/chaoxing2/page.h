﻿#ifndef PAGE_H
#define PAGE_H

#include <QWidget>
#include "navbutton.h"
#include "movable_widget.h"

namespace Ui {
class page;
}

class page : public movable_widget
{
    Q_OBJECT

public:
    explicit page(QWidget *parent = 0);
    ~page();
public slots:
    void buttonClick();
private:
    Ui::page *ui;
    QList<NavButton *> buttons;
};

#endif // PAGE_H
