﻿#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    pEngView = new QWebEngineView(this);
    pEngView->setContextMenuPolicy(Qt::NoContextMenu);

    pEngView->load(QUrl::fromLocalFile("D:\\Qt\\Qt5Book\\Qt5.9Samp\\echart\\echart\\testEcharts5.html"));
    //这句是调用本地html文件的地址，前面讲过要将testEcharts.html文件和main.echarts.js文件放在同一个文件夹htmlEcharts中。
    pEngView->resize(this->size());
    pEngView->show();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::resizeEvent(QResizeEvent *event)
{
    pEngView->resize(this->size());
}


