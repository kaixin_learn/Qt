﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "movable_widget.h"
#include <QButtonGroup>
namespace Ui {
class Form;
}

class Widget : public movable_widget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_max_clicked();


private:
    Ui::Form *ui;
    QButtonGroup btngroup;
};

#endif // WIDGET_H
