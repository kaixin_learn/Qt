﻿#include "widget.h"
#include "ui_mainpage.h"

Widget::Widget(QWidget *parent) :
    movable_widget(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);
    QList<QToolButton *> tbtns=ui->widget->findChildren<QToolButton *>();
    for(int i=0;i<tbtns.count();++i)
    {
        QToolButton * btn=tbtns.at(i);
        btngroup.addButton(btn,i+1);
        btn->setCheckable(true);
    }
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_max_clicked()
{
    if(this->isMaximized()){
        this->showNormal();
    }
    else{
        this->showMaximized();
    }
}
