﻿#include "movable_widget.h"
#include <QMouseEvent>
movable_widget::movable_widget(QWidget * parent):
    QWidget(parent)
{
    this->setWindowFlags(Qt::FramelessWindowHint);
}
void movable_widget::mouseMoveEvent(QMouseEvent *event)
{
    if(ismoving)
    {
        QPoint now=event->globalPos()-Pos;
        move(now);
    }
    QWidget::mouseMoveEvent(event);
}

void movable_widget::mousePressEvent(QMouseEvent *event)
{
    if(event->button()==Qt::LeftButton)
    {
            ismoving=true;
            Pos=event->globalPos()-pos();
    }
    QWidget::mousePressEvent(event);
}

void movable_widget::mouseReleaseEvent(QMouseEvent *event)
{
    ismoving=false;
    QWidget::mouseReleaseEvent(event);
}

