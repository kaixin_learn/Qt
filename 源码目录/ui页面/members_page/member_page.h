﻿#ifndef MEMBER_PAGE_H
#define MEMBER_PAGE_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class member_page; }
QT_END_NAMESPACE

class member_page : public QWidget
{
    Q_OBJECT

public:
    member_page(QWidget *parent = nullptr);
    ~member_page();
protected:
    void init_page();
private:
    Ui::member_page *ui;
};
#endif // MEMBER_PAGE_H
