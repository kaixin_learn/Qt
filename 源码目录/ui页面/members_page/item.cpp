﻿#include "item.h"
#include "ui_item.h"

item::item(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::item)
{
    ui->setupUi(this);
}

item::~item()
{
    delete ui;
}

void item::set_name1(QString name)
{
    ui->label_4->setText(name);
}

void item::set_name2(QString name)
{
    ui->label_5->setText(name);
}

void item::set_icon(QString icon)
{
    //:/img/bg.jpg
    QString path=QString("border-image: url(%1);").arg(icon);
    ui->label_6->setStyleSheet(path);
}
