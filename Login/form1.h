﻿#ifndef FORM1_H
#define FORM1_H

#include <QWidget>

namespace Ui {
class Login1;
}

class Form1 : public QWidget
{
    Q_OBJECT

public:
    explicit Form1(QWidget *parent = 0);
    ~Form1();

private:
    Ui::Login1 *ui;
};

#endif // FORM1_H
