﻿#ifndef FORM_H
#define FORM_H

#include <QWidget>
#include <QLabel>
namespace Ui {
class Widget;
}

class Form : public QWidget
{
    Q_OBJECT

public:
    explicit Form(QWidget *parent = 0);
    ~Form();
protected:
    void paintEvent(QPaintEvent *event);
private:
    Ui::Widget *ui;
    QLabel * lab;
};

#endif // FORM_H
