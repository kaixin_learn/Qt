﻿#include "form.h"
#include "ui_widget3.h"
#include <QPainter>
#include <QDebug>
Form::Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    lab=new QLabel(this);
    lab->resize(120,120);
    QPixmap pixmap(":/4.png");
    lab->setPixmap(pixmap);
    lab->setScaledContents(true);
    lab->show();
}

Form::~Form()
{
    delete ui;
}


void Form::paintEvent(QPaintEvent *event)
{
    QPoint p(ui->widget_3->pos()+ui->widget_2->pos());
    QPoint p1(p.x()+ui->widget_3->width()/2-60,p.y()-60);
    lab->move(p1);
}

