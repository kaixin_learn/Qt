﻿#ifndef MOVABLE_WIDGET_H
#define MOVABLE_WIDGET_H

#include <QWidget>

class movable_widget:public QWidget
{
public:
    movable_widget(QWidget * parent=0);
protected:
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
private:
    QPoint Pos;
    bool ismoving;
};

#endif // MOVABLE_WIDGET_H
