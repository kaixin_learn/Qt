﻿#include "page.h"
#include "ui_page.h"

page::page(QWidget *parent) :
    movable_widget(parent),
    ui(new Ui::page)
{
    ui->setupUi(this);
    //QList<QPushButton *> allPButtons = parentWidget.findChildren<QPushButton *>();
    buttons=ui->scrollAreaWidgetContents->findChildren<NavButton *>();
    QFont textfont;
    textfont.setFamily("黑体");
    textfont.setBold(true);
    textfont.setPointSize(11);

    for(int i=0;i<buttons.count();++i)
    {
        NavButton * btn=buttons.at(i);
        btn->setNormalBgColor(QColor(77, 88, 181));
        btn->setHoverBgColor(QColor(36, 57, 169));
        btn->setCheckBgColor(QColor(89, 101, 201));

        btn->setNormalTextColor(QColor(255,255,255));

        QPixmap iconNormal(":/images/iconfont-qicheyongpin.png");
        QPixmap iconHover(":/images/iconfont-qicheyongpin (1).png");
        QPixmap iconCheck(":/images/iconfont-qicheyongpin (1).png");
        btn->setTextFont(textfont);

        btn->setPaddingLeft(55);
        btn->setShowIcon(true);
        btn->setIconSpace(20);
        btn->setIconSize(QSize(25, 25));
        btn->setIconNormal(iconNormal);
        btn->setIconHover(iconHover);
        btn->setIconCheck(iconCheck);

        QPixmap icon2(":/images/右箭头 (1).png");
        btn->setShowIcon2(true);
        btn->setIcon2Space(250);
        btn->setIcon2Size(QSize(25, 25));
        btn->setIcon2Check(icon2);

        btn->setShowLine(false);

        //鼠标放在按钮上，变为手
        btn->setCursor(Qt::PointingHandCursor);

        connect(btn, SIGNAL(clicked(bool)), this, SLOT(buttonClick()));
    }
    //去边框
    //继承自movable_widget

    //窗口边框圆角
    this->setStyleSheet("QWidget{border-top-left-radius:30px;border-top-right-radius:30px;}");

    //关于按钮右边的箭头
}

page::~page()
{
    delete ui;
}

void page::buttonClick()
{
    NavButton * s=(NavButton *)sender();
    for (int i = 0; i < buttons.count(); i++)
    {
        NavButton *btn = buttons.at(i);
        if(btn !=s)btn->setChecked(false);
    }
}
