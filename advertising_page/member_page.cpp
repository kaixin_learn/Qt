﻿#include "member_page.h"
#include "ui_member_page.h"
#include "item.h"
#include <QDebug>
#pragma execution_character_set("utf-8")

member_page::member_page(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::member_page)
{
    ui->setupUi(this);
    init_page();
}

member_page::~member_page()
{
    delete ui;
}

void member_page::init_page()
{
    QList<QString> name1,name2,icon;
    qDebug()<<QString("文");
    name1<<QString("文档翻译")<<"音频翻译"<<"英文写作"<<"同传翻译";
    name2<<"100万字符/月"<<"畅享免费时长"<<"专业分析报告"<<"畅享免费时长";
    icon<<":/img/文件.png"<<":/img/音频.png"<<":/img/写作指导.png"<<":/img/翻译.png";
    QVector<item *> vector;
    vector<<ui->widget_2<<ui->widget_3<<ui->widget_4<<ui->widget_5;
    qDebug()<<name1;
    qDebug()<<name2;
    qDebug()<<icon;
    for(int i=0;i<4;i++)
    {
        item * one=vector.at(i);
        QString n1,n2,ic;
        n1=name1.at(i);
        n2=name2.at(i);
        ic=icon.at(i);
        one->set_name1(n1);
        one->set_name2(n2);
        one->set_icon(ic);
    }
}

