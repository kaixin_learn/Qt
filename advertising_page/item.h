﻿#ifndef ITEM_H
#define ITEM_H

#include <QWidget>

namespace Ui {
class item;
}

class item : public QWidget
{
    Q_OBJECT

public:
    explicit item(QWidget *parent = nullptr);
    ~item();
    void set_name1(QString name);
    void set_name2(QString name);
    void set_icon(QString icon);
private:
    Ui::item *ui;
};

#endif // ITEM_H
